<!doctype html>
<html>
<head>
<?php _widget('head'); ?>
</head>

<body class="<?php _widget('custom_paletteclass'); ?>">
<?php _widget('custom_palette'); ?>
<div id="wrapper">
    <div id="header-wrapper">
        <div id="header">
            <div id="header-inner">
                <?php 
                foreach($widgets_order->header as $widget_filename){
                    _widget($widget_filename);
                } 
                ?>
                
                
            </div><!-- /.header-inner -->
        </div><!-- /#header -->
    </div><!-- /#header-wrapper -->
    <div id="main-wrapper">
        <div id="main">
            <div id="main-inner">
                <!-- SLOGAN -->
                <?php 
                
                $conflict_widgets = array('top_companymap'=>1, 'top_map'=>1, 'top_slideshow'=>1);
                $conflict_active=false;
                
                foreach($widgets_order->top as $widget_filename){
                    if(!$conflict_active || !isset($conflict_widgets[$widget_filename]))
                        _widget($widget_filename);
                        
                    if(isset($conflict_widgets[$widget_filename]))
                    {
                        $conflict_active=true;
                    }
                } 
                ?>
                <div id="content" class="container"> 
                    <div class="block-content block-content-small-padding">
                        <div class="row">
                            <div class="col-sm-9">
                                <?php //<h2>{page_title}</h2> ?>            
                                
            <?php 
                foreach($widgets_order->center as $widget_filename){
                    _widget($widget_filename);
                } 
                ?> 
                            </div>
                            <div class="col-sm-3">
                                <div class="sidebar">
                                    <div class="sidebar-inner">                                  
            <?php 
            foreach($widgets_order->right as $widget_filename){
                _widget($widget_filename);
            } 
            ?> 
                                    </div><!-- /.sidebar-inner -->
                                </div><!-- /.sidebar -->
                            </div>
                        </div><!-- /.row -->
                    </div><!-- /.block-content -->
                    
            <?php 
                foreach($widgets_order->bottom as $widget_filename){
                    _widget($widget_filename);
                } 
                ?> 
                </div><!-- /.container -->
            </div><!-- /#main-inner -->
        </div><!-- /#main -->
    </div><!-- /#main-wrapper -->
    <div id="footer-wrapper">
        <div id="footer">
            <div id="footer-inner">
            <?php 
            foreach($widgets_order->footer as $widget_filename){
                _widget($widget_filename);
            } 
            ?> 
            </div><!-- /#footer-inner -->
        </div><!-- /#footer -->
    </div><!-- /#footer-wrapper -->
</div><!-- /#wrapper -->
<?php _widget('custom_javascript'); ?> 
</body>
</html>